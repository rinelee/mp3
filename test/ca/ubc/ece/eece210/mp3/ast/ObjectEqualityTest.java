package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Genre;

public class ObjectEqualityTest {
	
	
	static Album album1;
	static Album album2;
	static Album album3;
	static Album album4;
	static Album album5;
	static Album album6;
	static Object albumObject1;
	static Object albumObject2;
	static Genre genre1;
	static Genre genre2;
	static Genre genre3;
	static Object genreObject1;
	static Object genreObject2;
	
    
	
	
	@BeforeClass
	public static void setup(){
		
		//Create two array list to hold the songs.
		ArrayList<String> songs1 = new ArrayList<String>();
		ArrayList<String> songs2 = new ArrayList<String>();
		
		
		//Add songs to the song1.
		songs1.add("Ideal White");
		songs1.add("Another Heaven");
		songs1.add("Madder Red Town");
		songs1.add("Ever Present Feeling");
		

		//Add songs to the song2.
		songs2.add("Madder Red Town");
		songs2.add("Ideal White");
		songs2.add("Ever Present Feeling");
		songs2.add("Another Heaven");
		
		
		
		//Create some albums.
		album1 = new Album("Fate/Stay Night", "Type-Moon", 2004, songs1);
		album2 = new Album("Fate/Stay Night", "Type-Moon", 2004, songs1);
		album3 = new Album("Type-Moon", "Fate/Stay Night", 2004, songs2);
		album4 = new Album("Fate/Stay Night", "Type-Moon", 2004, songs2);
		album5 = new Album(null,null,0,null);
		album6 = new Album(null,null,0,null);
		
		//Create some genres.
		genre1 = new Genre("Ballads");
		genre2 = new Genre("Visual Novels");
		genre3 = new Genre("Ballads");
		
		
		
		//Set the genres of the albums.
		album1.setGenre(genre2);
		album2.setGenre(genre2);
		album3.setGenre(genre2);
		
	
	}
	
	//Test if two albums are equal.
	@Test
	public void testForAlbumEquality() {
		
		assertTrue(album1.equals(album2));
		assertTrue(album1.equals(album4));
		assertFalse(album1.equals(album3));
		assertFalse(album2.equals(album3));
		
	}
	
	//Test if two genres are equal.
	@Test
	public void testForGenreEquality() {
			
		assertTrue(genre1.equals(genre3));
		assertFalse(genre1.equals(genre2));
		assertFalse(genre3.equals(genre2));
			
	}
	
	//Test if genres are not equal to albums and vice-versa.
	@Test
	public void testForAlbumAndGenreEquality() {
				
		assertFalse(album1.equals(genre2));
		assertFalse(genre3.equals(album2));
				
	}
	
	//Test if album objects are equal.
	@Test
	public void testForAlbumObjectsEquality() {
		
		albumObject1 = (Object) album1;
		albumObject2 = (Object) album2;
		
		assertTrue(albumObject1.equals(albumObject2));
		
					
	}
	
	//Test if genre objects are equal.
	@Test
	public void testForGenreObjectsEquality() {
		
		genreObject1 = (Object) genre1;
		genreObject2 = (Object) genre3;
			
		assertTrue(genreObject1.equals(genreObject2));
			
						
	}
	
	//Test if null albums are equal.
	@Test
	public void testForNullAlbumsEquality() {
		
			
		assertTrue(album5.equals(album6));
		assertFalse(album5.equals(album1));	
						
	}
	
	//Test if album and genre are not equal when compared to null.
	@Test
	public void testForNullEquality() {
			
				
		assertFalse(album1.equals(null));
		assertFalse(genre1.equals(null));	
							
	}
	
	

}
