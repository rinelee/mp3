package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

public class CatalogueQueryTest {
	   
	static Album album1;
	static Album album2;
	static Album album3;
	static Album album4;
	static Genre genre1;
	static Genre genre2;
	static Catalogue catalogue1;
	
	@BeforeClass
	public static void setup(){
		catalogue1 = new Catalogue();
		ArrayList<String> songs = new ArrayList<String>();
		
		//Add songs to the songlist.
		songs.add("Ideal White");
		songs.add("Another Heaven");
		songs.add("Madder Red Town");
		songs.add("Ever Present Feeling");
		
		//Create some albums and genres.
		album1 = new Album("Fate/Stay Night", "Type-Moon", 2004, songs);
		album2 = new Album("Mahoyo", "Type-Moon", 1995, songs );
		album3 = new Album("Code Geass", "Sunrise", 2013, songs);
		album4 = new Album("Fate/Hollow Ataraxia", "Type-Moon", 2005, songs);
		
		genre1 = new Genre("Ballads");
		genre2 = new Genre("Visual Novels");
		
		catalogue1.add(album1);
		catalogue1.add(album2);
		catalogue1.add(album3);
		catalogue1.add(genre1);
		catalogue1.add(genre2);
		catalogue1.add(album4);
		
		album1.setGenre(genre2);
		album2.setGenre(genre2);
		album3.setGenre(genre1);
		
	
	}
	
	//Test all leaf nodes.
	@Test
	public void testIn() {
		List<Album> albumsFound = catalogue1.query("in (\"Visual Novels\")");
		
		assertTrue(albumsFound.contains(album2) && albumsFound.contains(album1));
		assertFalse(albumsFound.contains(album3));
		
		genre2.addToGenre(genre1);
		albumsFound = catalogue1.query("in (\"Visual Novels\")");
		
		assertTrue(albumsFound.contains(album2) && albumsFound.contains(album1) && albumsFound.contains(album3));
	}
	
	@Test
	public void testBy() {
		List<Album> albumsFound = catalogue1.query("by (\"Type-Moon\")");
		
		assertTrue(albumsFound.contains(album1));
		assertTrue(albumsFound.contains(album2));
		assertFalse(albumsFound.contains(album3));
	}
	
	@Test
	public void testMatches(){
		List<Album> albumsFound = catalogue1.query("matches (\"Fate/Stay Night\")");
		
		assertTrue(albumsFound.contains(album1));
		assertFalse(albumsFound.contains(album2));
		
	}
	
	@Test
	public void testRegexMatches(){
		List<Album> albumsFound = catalogue1.query("matches (\".*a.*\")");
		
		assertTrue(albumsFound.contains(album1) && albumsFound.contains(album2) && albumsFound.contains(album3));
	
	}
	
	@Test
	public void testBefore(){
		List<Album> albumsFound = catalogue1.query("before (\"2004\")");
		
		assertTrue(albumsFound.contains(album2));
		assertFalse(albumsFound.contains(album1) || albumsFound.contains(album3));
	}
	
	@Test
	public void testAfter(){
		List<Album> albumsFound = catalogue1.query("after (\"1995\")");
		
		assertFalse(albumsFound.contains(album2));
		assertTrue(albumsFound.contains(album1) && albumsFound.contains(album3));
	}
	
	//Test non-leaf nodes
	@Test
	public void testAnd(){
		List<Album> albumsFound = catalogue1.query("by (\"Type-Moon\") && matches (\"Fate/Stay Night\")");
		
		assertTrue(albumsFound.contains(album1));
		assertFalse(albumsFound.contains(album2));
	}

	@Test
	public void testOr(){
		List<Album> albumsFound = catalogue1.query("by (\"Sunrise\") || matches (\"Fate/Stay Night\")");
		
		assertTrue(albumsFound.contains(album3));
		assertTrue(albumsFound.contains(album1));
		assertFalse(albumsFound.contains(album2));
	}

	//Test complex expressions
	@Test
	public void testNestedAndOr(){
		List<Album> albumsFound = catalogue1.query("(by (\"Type-Moon\") && matches (\"Fate/Stay Night\")) || matches (\"Mahoyo\")");
		
		assertTrue(albumsFound.contains(album2));
		assertTrue(albumsFound.contains(album1));
		assertFalse(albumsFound.contains(album3));
		
		
	}
	
	@Test 
	public void testBeforeAndOr(){
		List<Album> albumsFound = catalogue1.query("(in (\"Visual Novels\") && by (\"Type-Moon\") && before (\"2004\")) || matches (\".*Geass\")");
		
		assertTrue(albumsFound.contains(album3));
		assertTrue(albumsFound.contains(album2));
		assertFalse(albumsFound.contains(album1));
	}
	
	
	@Test
	public void testAllFalse(){
		List<Album> albumsFound = catalogue1.query("after (\"2004\") || matches (\".*Cat.*\") || before (\"1995\") || in (\"Cat Vids\") || by (\"Bunnies\")");
		
		assertFalse(albumsFound.contains(album1) && albumsFound.contains(album2) && albumsFound.contains(album3));
	}
	
	//Boundary cases
	@Test
	public void testEmpty(){
		try{
			@SuppressWarnings("unused")
			List<Album> albumsFound = catalogue1.query("after (\"\") || in (\"\") || before (\"\") || by (\"\") || matches (\"\") ");
			fail("Should throw exception");
		} catch (Exception e) {
			assertTrue(e.getClass() == IllegalArgumentException.class);
		}
	}
	
	
	@Test
	public void testUnclassifiedGenre(){
		List<Album> albumsFound = catalogue1.query("in (\"Unclassified\")");
	
		assertTrue(albumsFound.contains(album4));
	}

}
