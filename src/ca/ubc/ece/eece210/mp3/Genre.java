package ca.ubc.ece.eece210.mp3;

import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * Represents a genre (or collection of albums/genres).
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Genre extends Element {

	// Representation invariant:
	// title is not null and not an empty String.

	private static final String openTag = "<genre>";
	private static final String closeTag = "</genre>";
	private static final String openNameTag = "<name>";
	private static final String closeNameTag = "</name>";

	// The Genre name
	String title;
	
	//Catalogue
	private Catalogue catalogue;

	/**
	 * Creates a new genre with the given name.
	 * 
	 * @param name
	 *            the name of the genre.
	 */
	public Genre(String name) {
		if (name.equals("")) {
			throw new IllegalArgumentException(
					"The Genre title cannot be an empty string.");
		}
		title = name;
		catalogue = null;
	}

	/**
	 * Returns the genre name.
	 * 
	 * @return genre name.
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Place the genre in a catalogue. 
	 * 
	 * @requires
	 * 		the genre's catalogue has not already been set as another. Note: this
	 * 		method does not place the genre into a catalogue, merely notes that it
	 * 		will be placed in this catalogue.
	 * 
	 * @param catalogueToPlace
	 * 		the catalogue the element will placed as a part of.
	 */
	public void setCatalogue(Catalogue catalogueToPlace){
		catalogue = catalogueToPlace; //An element cannot be removed from a catalogue so it's safe to assume that we don't need methods to remove an genre.
	}
	
	
	/**
	 * Returns the catalogue the genre belongs to.
	 * 
	 * @return
	 * 		the catalogue the genre is an element of, null if it belongs to no catalogue.
	 */
	public Catalogue getCatalogue(){
		return catalogue;
	}
	
	/**
	 * Determines the hash code of the genre.
	 */
	@Override 
	public int hashCode(){
		int hashFactor = 4869;
		return title.hashCode()*hashFactor;
	}
	
	/**
	 * Tests the genre for equality.
	 * 
	 * @param genre
	 * 		 	the genre to compare with this genre.
	 * @return
	 * 			true if the genres are equal, false otherwise.
	 */
	public boolean equals(Genre genre){
		//Case 0: The object to match with is null.
		if(genre == null){
			return false;
		}
		
		//Case 1: The objects are aliases (point to the same object).
		if(genre == this){
			return true;
		}
		//Case 2: The objects do not have the same hash code and cannot be equal.
		if(genre.hashCode() != hashCode()){
			return false;
		}
		
		
		//Case 3: The titles are not the same and thus cannot be equal.
		if(genre.getTitle() != title){
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public boolean equals(Object obj){
		//Objects are not the same if one is null.
		if(obj==null){
			return false;
		}
		
		//The objects are not the same class and cannot be equal.
		if(obj.getClass() != getClass()){
			return false;
		}
		
		return equals((Genre) obj);
	}
	

	/**
	 * Restores a genre from its given string representation.
	 * 
	 * @param stringRepresentation
	 */
	public static Genre restoreCollection(String stringRepresentation) {
		CharStream stream = new ANTLRInputStream(stringRepresentation);
		CatalogueLexer lexer = new CatalogueLexer(stream);
		TokenStream tokens = new CommonTokenStream(lexer);
		
		CatalogueParser parser = new CatalogueParser(tokens);
		ParseTree tree = parser.root();
		((RuleContext) tree).inspect(parser);
		
		
		ParseTreeWalker walker = new ParseTreeWalker();
		CatalogueListenerCatalogueCreator listener = new CatalogueListenerCatalogueCreator();
		walker.walk(listener, tree);
		Catalogue c = listener.getCatalogue();

		if ((c.size() == 1) && (c.get(0).hasChildren())) {
			// there was exactly one element in the String and it was a Genre
			return (Genre) c.get(0);
		} else {
			throw new IllegalArgumentException(
					"The input string is not a valid representation of a genre.");
		}
		
	}

	/**
	 * Returns the string representation of a genre. The string representation
	 * is: <album><br />
	 * album title <br />
	 * <b><performer></b> the performer's name <b></performer></b><br />
	 * <b><song></b> song 1 <b></song></b><br />
	 * <b><song></b> song 2 <b></song></b><br />
	 * ... <b></album></b><br />
	 * 
	 * @return the string representation.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(openTag + "\n");
		sb.append(openNameTag + title + closeNameTag + "\n");
		List<Element> listOfChildren = getChildren();
		for (Element e : listOfChildren) {
			// System.out.println(e.toString());
			sb.append(e.toString());
		}
		sb.append(closeTag + "\n");
		return sb.toString();
	}

	/**
	 * Adds the given album or genre to this genre
	 * 
	 * @param b
	 *            the element to be added to the collection.
	 */
	public void addToGenre(Element b) {
		// Delegate to proper add method
		if (b instanceof Album) {
			addToGenre((Album)b);
		} else if (b instanceof Genre) {
			addToGenre((Genre)b);
		} else {
			addChild(b);
		}
	}
	
	public void addToGenre(Album a) {
		a.setGenre(this);
	}
	
	public void addToGenre(Genre subgenre) {
		addChild(subgenre);
	}

	/**
	 * Returns true, since a genre can contain other albums and/or genres.
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}
}