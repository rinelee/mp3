package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

public class MatchesNode extends ASTNode {

    public MatchesNode(Token token) {
	super(token);
    }

    /**
	 * Interpret/evaluate an MatchesNode of a query over a given catalogue. Requires catalogue to not be null.
	 * 
	 * @param catalogue
	 * 			the catalogue to evaluate the arguments over.
	 * @return 
	 * 			a set of albums with titles that match the specified arguments.
	 * @throws IllegalArgumentException 
	 * 			if arguments string is empty.
	 */
    
    @Override
    public Set<Element> interpret(Catalogue argument) throws IllegalArgumentException {
    	if(arguments.isEmpty()){
    		throw new IllegalArgumentException();
    	}
    	else{
    		return argument.searchByTitle(arguments);
    	}
    }

}

