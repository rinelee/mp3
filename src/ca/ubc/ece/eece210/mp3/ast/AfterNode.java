package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;

import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;

public class AfterNode extends ASTNode{
	/**
	 * Create a new AfterNode given a parser token
	 * 
	 * @param token
	 */
	public AfterNode(Token token) {
		super(token);
	}

	/**
	 * Interpret/evaluate an AfterNode of a query over a given catalogue. Requires catalogue to not be null.
	 * 
	 * @param catalogue
	 * 			the catalogue to evaluate the arguments over
	 * @return 
	 * 			a set of albums that were released after a 
	 * 			specified year, corresponding to the arguments.
	 * @throws IllegalArgumentException 
	 * 			if arguments string is empty.
	 */
	@Override
	public Set<Element> interpret(Catalogue catalogue) throws IllegalArgumentException{
		if(arguments.isEmpty()){
			throw new IllegalArgumentException();
		}
		else{
		int convertedArgs = Integer.parseInt(arguments);
		return catalogue.searchAfterYear(convertedArgs);
		}
	}

}
