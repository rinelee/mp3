package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

public class OrNode extends ASTNode {

    public OrNode(Token token) {
	super(token);
    }
    
    /**
	 * Interpret/evaluate an OrNode of a query over a given catalogue. Requires catalogue to not be null.
	 * 
	 * @param catalogue
	 * 			the catalogue to evaluate the arguments over.
	 * @return 
	 * 			a set of albums that is true for either the left 
	 * 			or right arguments of the OrNode.
	 * @requires 
	 * 			there are always both left and right arguments to interpret. 
	 * 			( A || B ) works but ( A || B || ) does not.
	 */
    
    @Override
    public Set<Element> interpret(Catalogue argument) {
		Set<Element> leftArgumentResults = children.get(0).interpret(argument); //Assuming there will always be two arguments.
		Set<Element> rightArgumentResults = children.get(1).interpret(argument);
		HashSet<Element> finalList = new HashSet<Element>();
		
		addElementsToList(finalList, leftArgumentResults); //Mutates the list accordingly.
		addElementsToList(finalList, rightArgumentResults);
		
		return finalList; //Return an empty list if there is nothing.
	}
    
    //TODO specs -- do not use Javadoc
    private void addElementsToList(HashSet<Element> finalList, Set<Element> listToInclude){
    	Iterator<Element> checkElements = listToInclude.iterator();
    	
    	while(checkElements.hasNext()){
    		Element album = checkElements.next();
    		finalList.add(album); //Since HashSet only saves unique copies of values, no need to check.
    	}
    }

}