package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;

import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;

public class ByNode extends ASTNode {
	
	/**
	 * Create a new ByNode given a parser token
	 * 
	 * @param token
	 */
	public ByNode(Token token) {
		super(token);
	}

	/**
	 * Interpret/evaluate an ByNode of a query over a given catalogue. Requires catalogue to not be null.
	 * 
	 * @param catalogue
	 * 			the catalogue to evaluate the arguments over
	 * @return 
	 * 			a set of albums that are by a specified performer 
	 * 			corresponding to the arguments.
	 * @throws IllegalArgumentException 
	 * 			if arguments string is empty.
	 */
	@Override
	public Set<Element> interpret(Catalogue catalogue) throws IllegalArgumentException {
		if(arguments.isEmpty()){
			throw new IllegalArgumentException();
		}	
		else {
			return catalogue.searchByPerformer(arguments);
		}
	}

}
