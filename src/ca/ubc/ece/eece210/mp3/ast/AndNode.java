package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 */

public class AndNode extends ASTNode {

	/**
	 * Create a new AndNode given a parser token
	 * 
	 * @param token
	 */
	public AndNode(Token token) {
		super(token);
	}

	/**
	 * Interpret/evaluate an AndNode of a query over a given catalogue. Requires catalogue to not be null.
	 * 
	 * @param catalogue
	 * 			the catalogue to evaluate the arguments over.
	 * @return 
	 * 			a set of albums that is true for both the left and 
	 * 			right arguments of the AndNode.
	 * @requires 
	 * 			there are always both left and right arguments to interpret. 
	 * 			( A && B ) works but ( A && B && ) does not.
	 */
	
	@Override
	public Set<Element> interpret(Catalogue catalogue) {
		Set<Element> leftArgumentResults = children.get(0).interpret(catalogue); //Assuming there will always be two arguments.
		Set<Element> rightArgumentResults = children.get(1).interpret(catalogue);
		HashSet<Element> finalList = new HashSet<Element>();
		
		Iterator<Element> checkElements = leftArgumentResults.iterator();
		
		while(checkElements.hasNext()){
			Element currentAlbum = checkElements.next(); 
			if(rightArgumentResults.contains(currentAlbum)){
				finalList.add(currentAlbum); //Include any elements common to both lists.
			}
		}
		
		return finalList; //Return an empty list if there is nothing.
	}

}
