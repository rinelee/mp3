package ca.ubc.ece.eece210.mp3;

import java.util.List;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 *         This class contains the information needed to represent an album in
 *         our application.
 * 
 */

public final class Album extends Element {

	// Representation invariant:
	// (1) title, performer, songList and genreName are not null.
	// (2) if parentGenre is set then Album is also a child of the appropriate
	// Genre object.

	private String title;
	private String performer;
	private List<String> songList;
	private int year;
	private Genre parentGenre;
	private Catalogue catalogue;
	
	private final String noGenreName = "Unclassified";

	private static final String openTag = "<album>";
	private static final String closeTag = "</album>";
	private static final String openNameTag = "<name>";
	private static final String closeNameTag = "</name>";
	private static final String openPerformerTag = "<performer>";
	private static final String closePerformerTag = "</performer>";
	private static final String openYearTag = "<year>";
	private static final String closeYearTag = "</year>";
	private static final String openSongTag = "<song>";
	private static final String closeSongTag = "</song>";

	/**
	 * Builds an album with the given title, performer and song list
	 * 
	 * @param title
	 *            the title of the album
	 * @param performer
	 *            the performer
	 * @param songlist
	 *            the list of songs in the album
	 */
	public Album(String title, String performer, int year, List<String> songlist) {

		this.title = title;
		this.performer = performer;
		this.songList = songlist;
		this.year = year;
		this.parentGenre = null;  // "Unclassified";
		this.catalogue = null; //No catalogue to start with.
	}
	
	/**
	 * Place the album in a catalogue. 
	 * 
	 * @requires
	 * 		the album's catalogue has not already been set as another. Note: this
	 * 		method does not place the album into a catalogue, merely notes that it
	 * 		will be placed in this catalogue.
	 * 
	 * @param catalogueToPlace
	 * 		the catalogue the element will placed as a part of.
	 */
	public void setCatalogue(Catalogue catalogueToPlace){
		catalogue = catalogueToPlace; //An element cannot be removed from a catalogue so it's safe to assume that we don't need methods to remove an album. 
		catalogue.addAlbumPerformer(performer, this);
		catalogue.addAlbumTitle(title, this);
		catalogue.addAlbumYear(year, this);
		catalogue.addAlbumGenre(parentGenre, this); //Add to genre "Unclassified" if there is no genre.
	}
	
	/**
	 * Returns the catalogue the album belongs to.
	 * 
	 * @return
	 * 		the catalogue the album is an element of, null if it belongs to no catalogue.
	 */
	public Catalogue getCatalogue(){
		return catalogue;
	}

	

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file.
	 * 
	 * @param stringRepresentation
	 *            the string representation
	 */
	public Album(String stringRepresentation) {

		CharStream stream = new ANTLRInputStream(stringRepresentation);
		CatalogueLexer lexer = new CatalogueLexer(stream);
		TokenStream tokens = new CommonTokenStream(lexer);

		CatalogueParser parser = new CatalogueParser(tokens);
		ParseTree tree = parser.root();
		((RuleContext) tree).inspect(parser);

		ParseTreeWalker walker = new ParseTreeWalker();
		CatalogueListenerCatalogueCreator listener = new CatalogueListenerCatalogueCreator();
		walker.walk(listener, tree);
		Catalogue c = listener.getCatalogue();

		if ((c.size() == 1) && (!c.get(0).hasChildren())) {
			// there was exactly one element in the String and it was an Album
			Album album = (Album) c.get(0);
			this.title = album.title;
			this.performer = album.performer;
			this.year = album.year;
			this.songList = album.songList;
			this.parentGenre = null;  // must be set by adding to a genre
		} else {
			throw new IllegalArgumentException(
					"The input string is not a valid representation of an album.");
		}
	}
	
	
	/**
	 * Remove album from supplied genre (if present)
	 * @param genre
	 * 		the genre to remove the album from
	 * @return
	 * 		true if successfully removed
	 */
	public boolean removeFromGenre(Genre genre) {
		boolean removed = false;
		if (genre == parentGenre) {
			removed = parentGenre.removeChild(this);
			catalogue.changeGenre(noGenreName, parentGenre.getTitle(), this);
			parentGenre = null;
			
		}
		return removed;
		
	}

	/**
	 * Add the album to the given genre
	 * 
	 * @param genre
	 *            the genre to add the album to.
	 */
	public void setGenre(Genre genre) {
		
		// add to new genre
		if (genre != null) {
			// remove from old genre first (prevents duplicates)
			if (parentGenre != null) {
				removeFromGenre(parentGenre);
			}
			
			if(catalogue != null){
				catalogue.changeGenre(genre.getTitle(), noGenreName, this);
			}
			genre.addChild(this);
			parentGenre = genre;
			// System.out.println(genreName);
		}
	}

	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to
	 */
	public Genre getGenre() {
		return parentGenre;
	}

	/**
	 * Set the album title. Permits setting a title and renaming album title.
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the title of the album
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the performer of the album
	 * 
	 * @return the performer
	 */
	public String getPerformer() {
		return performer;
	}

	/**
	 * An album cannot have any children (it cannot contain anything).
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}
	


	/**
	 * Determines the hash code of an album.
	 */
	@Override
	public int hashCode() {
		//Create a hashCode from the sum of the performer and title's hash codes.
		int result = 0;
		int hashFactor = 1412;
		
		if(performer != null){
			result += performer.hashCode()*hashFactor;
		}
		
		if(title != null){
			result += title.hashCode()*hashFactor;
		}
	
		return result;
	}

	/**
	 * Determines if the album is equal to this album.
	 * 
	 * @param album
	 * 			the album to test for equality.
	 * @return
	 * 			true if the albums are equal, false otherwise.
	 */
	public boolean equals(Album album){
		//Based on auto-generated code from eclipse.
		//Case 0: If the object to match with is null, it cannot be a match.
		if(album == null){
			return false;
		}
		
		//Case 1: The object is simply an alias of the album.
		if(this == album){
			return true;
		}
		
		//Case 2: If the hashCodes are not the same, then the objects are not equal.
		if(hashCode() != album.hashCode()){
			return false;
		}
				
				
		//Case 3: The performers are not the same, so they are not equal.
		if(album.getPerformer() != performer){
				return false;
		}
				
		//Case 4: The titles are not the same, so they are not equal.
		if(album.getTitle() != title){
				return false;
		}
				
		//An album is equal if the titles and performers are equal.
		return true;
		
	}
	/**
	 * Determines if an object is equal to this album.
	 * 
	 * @param obj
	 * 			the object to test for equality.
	 * @return
	 * 			true if the object is equal to this album, false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		//Equals method based on the suggestion provided by Sathish on Piazza.
		
		//If the other object is null, then the objects are not equal.
		if(obj == null){
			return false;
		}
		
		//If the classes are not the same, then the objects are not equal
		if(obj.getClass() != getClass()){
			return false;
		}
		
		return equals((Album) obj);
		
	}
	
	/**
	 * Returns the string representation of the given album. The representation
	 * contains the title, performer and songlist.
	 * 
	 * @return the string representation
	 */
	public String toString() {
		StringBuilder sb;
		sb = new StringBuilder(openTag + "\n");
		sb = sb.append(openNameTag + title + closeNameTag + "\n");
		sb = sb.append(openPerformerTag + performer + closePerformerTag + "\n");
		sb = sb.append(openYearTag + year + closeYearTag + "\n");
		for (String song : songList) {
			sb = sb.append(openSongTag + song + closeSongTag + "\n");
		}
		sb = sb.append(closeTag + "\n");
		return sb.toString();
	}

}
