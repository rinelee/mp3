package ca.ubc.ece.eece210.mp3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import ca.ubc.ece.eece210.mp3.ast.ASTNode;
import ca.ubc.ece.eece210.mp3.ast.QueryParser;
import ca.ubc.ece.eece210.mp3.ast.QueryTokenizer;
import ca.ubc.ece.eece210.mp3.ast.Token;

/**
 * Container class for all the albums and genres. Its main responsibility is to
 * save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {

	private List<Element> contents;
	
	//Save all the albums of a catalogue according to group to retrieve for a query.
	private HashMap<String, HashSet<Element>> albumsInGenre;
	private HashMap<String, HashSet<Element>> albumsByPerformer;
	private HashMap<String, HashSet<Element>> albumsByTitle;
	private HashMap<Integer, HashSet<Element>> albumsByYear;
	private HashMap<String, Genre> listOfGenres;

	private TreeSet<Integer> setOfYears; //Keep a sorted record of years.
	
	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		//Initialize all storages.
		contents = new ArrayList<Element>();
		albumsInGenre = new HashMap<String, HashSet<Element>>();
		albumsByPerformer = new HashMap<String, HashSet<Element>>();
		albumsByTitle = new HashMap<String, HashSet<Element>>();
		albumsByYear = new HashMap<Integer, HashSet<Element>>();
		listOfGenres = new HashMap<String, Genre>();
		
		setOfYears = new TreeSet<Integer>();
	}
	
	/**
	 * Searches for a query and returns a list of albums reflected by the query.
	 * 
	 * @param queryString
	 * 			the string, in query language, to search for.
	 * @return
	 * 			a list of all the albums in the catalogue that match the given query.
	 */
	public List<Album> query(String queryString){
		//Assumes that the queryString is in the correct format.
		//Code based on the tests provided in QueryParserTest.java.
		//All items searched should be albums.
		
		List<Token> tokens = QueryTokenizer.tokenizeInput(queryString);
		QueryParser parser = new QueryParser(tokens);
		ASTNode rootNode = parser.getRoot();
		
		Set<Element> foundValues = rootNode.interpret(this);
		Iterator<Element> checkValues = foundValues.iterator();
		List<Album> finalList = new ArrayList<Album>(); //Convert the set's values to a list.
		
		while(checkValues.hasNext()){
			Element currAlbum = checkValues.next();
			
			if(currAlbum.getClass() == Album.class){
				finalList.add((Album) currAlbum);
			}
			
		}
		
		
		return finalList;
		
	}
	
	/**
	 * Adds a given album to its given genre. If the genre is null,
	 * the album is stored under "Unclassified".
	 * 
	 * @param genre
	 * 			the genre parent of the album
	 * @param album
	 * 			the album to be added to the specified genre
	 */
	public void addAlbumGenre(Genre genre, Album album){
		HashSet<Element> listOfAlbums;
		String genreName;
		
		if(genre == null){
			genreName = "Unclassified"; //Add to unclassified if there is no genre.
		}
		else{
			genreName = genre.getTitle();
		}
		
		if(albumsInGenre.containsKey(genreName)){
			listOfAlbums = albumsInGenre.get(genreName);
			listOfAlbums.add(album);
		}
		
		else{
			listOfAlbums = new HashSet<Element>();
			listOfAlbums.add(album);
		
			listOfGenres.put(genreName, genre);
		}
	
		albumsInGenre.put(genreName, listOfAlbums);
		
	}
	
	/**
	 * Updates a genre change. Note: this method does not actually
	 * change the genre. It merely updates records for a query search.
	 * 
	 * @param newGenre
	 * 			the name of the new genre the album is placed in.
	 * @param oldGenre
	 * 			the name of the old genre the album was placed in.
	 * @param album
	 * 			the album undergoing the genre change.
	 */
	public void changeGenre(String newGenre, String oldGenre, Album album){
		if(!albumsInGenre.containsKey(oldGenre)){
			throw new IllegalArgumentException("Old genre doesn't exist.");
		}
		
		HashSet<Element> oldListAlbums = albumsInGenre.get(oldGenre);
		
		if(!oldListAlbums.contains(album)){
			throw new IllegalArgumentException("The album isn't part of this genre.");
		}
		
		oldListAlbums.remove(album);
		addAlbumGenre(listOfGenres.get(newGenre), album); //The new genre must be a part of the catalogue already
														  //so we do not need to check and add it.
		
	}
	
	
	/**
	 * Stores information on an album's year.
	 * 
	 * @param year 
	 * 			the year the given album was released.
	 * @param album
	 * 			the album the year corresponds to.
	 */
	public void addAlbumYear(int year, Album album){
		HashSet<Element> listOfAlbums;
		
		if(albumsByYear.containsKey(year)){
			listOfAlbums = albumsByYear.get(year);
			listOfAlbums.add(album);
		}
		
		else{
			setOfYears.add(year);
			listOfAlbums = new HashSet<Element>();
			listOfAlbums.add(album);
		}
	
		albumsByYear.put(year, listOfAlbums);
		
	}
	
	/**
	 * Stores an album under a given performer.
	 * 
	 * @param performer
	 * 			the performer of an album.
	 * @param album
	 * 			the album in question.
	 */
	public void addAlbumPerformer(String performer, Album album){
		HashSet<Element> listOfAlbums;
		
		if(albumsByPerformer.containsKey(performer)){
			listOfAlbums = albumsByPerformer.get(performer);
			listOfAlbums.add(album);
		}
		
		else{
			listOfAlbums = new HashSet<Element>();
			listOfAlbums.add(album);
		}
	
		albumsByPerformer.put(performer, listOfAlbums);
		
	}
	
	/**
	 * Stores an album under the given title.
	 * A new title key is made if there is not already a title of the same name.
	 * @param title
	 * 			the title key to store the album value with
	 * @param album
	 * 			the album to store under the given title key
	 */
	public void addAlbumTitle(String title, Album album){
		HashSet<Element> listOfAlbums;
		
		if(albumsByTitle.containsKey(title)){
			listOfAlbums = albumsByTitle.get(title);
			listOfAlbums.add(album);
		}
		
		else{
			listOfAlbums = new HashSet<Element>();
			listOfAlbums.add(album);
		}
		
		albumsByTitle.put(title, listOfAlbums);
		
	}
	
	/**
	 * Returns all the albums that were released before the given year, exclusively.
	 * 
	 * @param year
	 * 			the threshold year
	 * @return a set of albums that were released before the threshold year
	 */
	public Set<Element> searchBeforeYear(int year){
		Iterator<Integer> checkYears = setOfYears.iterator();
		Set<Element> albums = new HashSet<Element>();
		
		while(checkYears.hasNext()){
			int currYear = checkYears.next();
			
			if(currYear < year){
				albums.addAll(albumsByYear.get(currYear));
			}
		}
		
		return albums;
	}
	
	/**
	 * Returns all the albums that were released after the given year, exclusively.
	 * 
	 * @param year
	 * 			the threshold year 
	 * @return a set of albums that were released after the threshold year
	 */
	public Set<Element> searchAfterYear(int year){
		Iterator<Integer> checkYears = setOfYears.iterator();
		Set<Element> albums = new HashSet<Element>();
		
		while(checkYears.hasNext()){
			int currYear = checkYears.next();
			
			if(currYear > year){
				albums.addAll(albumsByYear.get(currYear));
			}
		}
		
		return albums;
	}
	/**
	 * Returns the set of albums by a given performer.
	 * 
	 * @param performer
	 * 			the performer for which to search the albums. 
	 * @return 
	 * 			a set of album elements by the specified performer.
	 */
	public Set<Element> searchByPerformer(String performer){
		
		if(albumsByPerformer.containsKey(performer)){
			return albumsByPerformer.get(performer);
		}
		
		return new HashSet<Element>(); //Return an empty album if nothing was found.
	}
	
	/**
	 * Returns the albums whose titles match the given RegEx string. 
	 * 
	 * @param title 
	 * 			the string in RegEx form that will be used to find the matching albums.
	 * @return setofAlbums
	 * 			the set of albums that have titles that match the RegEx string. If the 
	 * 			provided string matches a title exactly, partial matches are disregarded,
	 * 			and only albums that correspond to the exact match.
	 */
	public HashSet<Element> searchByTitle(String title){
		
		HashSet<Element> setofAlbums = new HashSet<Element>();
		Set<String> titleKeys = albumsByTitle.keySet();
		Iterator<String> iterator = titleKeys.iterator();
		
		if(albumsByTitle.containsKey(title)){
			return albumsByTitle.get(title);
		}
		
		else{
			while(iterator.hasNext()){
				String next = iterator.next();
				if(next.matches(title)){
					setofAlbums.addAll(albumsByTitle.get(next));
				}
			}
			return setofAlbums;
		}
	}
	
	/**
	 * Returns a set of all album elements under the given genre name. Returns an empty 
	 * list if the genre name is not found in catalogue.
	 * 
	 * @param genreName
	 * 			the genre name string to search the catalogue with.
	 * @return qualifyingAlbums
	 * 			a set containing all the albums under the given genre name and its subgenres.
	 */
	public HashSet<Element> searchByGenre(String genreName){
		HashSet<Element> qualifyingAlbums = new HashSet<Element>();
		
		if(genreName.equals("Unclassified")){
			return albumsInGenre.get(genreName);
		}
		
		if(!listOfGenres.containsKey(genreName)){
			return qualifyingAlbums; //Empty list if the genre isn't even a part of the catalogue.
		}
		
		Genre genre = listOfGenres.get(genreName); //Retrieve the genre.

		if(albumsInGenre.containsKey(genreName)){
			
			if(genre.hasChildren()){ //Include albums in sub genres too.
				List<Element> subElements = genre.getChildren(); 
				
				for(Element e: subElements){
					if(e.getClass() == Album.class){
						qualifyingAlbums.add(e);
					}
					
					else if(e.getClass() == Genre.class){ 
						HashSet<Element> subElementsChild = searchByGenre(((Genre)e).getTitle()); //Retrieves all qualifying albums.
						qualifyingAlbums.addAll(subElementsChild);
					}
				}
			}
			
		}
		
		return qualifyingAlbums;
		
	}

	/**
	 * 
	 * @return
	 * 		the number of elements stored in this catalogue.
	 */
	public int size() {
		return contents.size();
	}
	       
	/**
	 * Retrieves a specific element from the catalogue.
	 * 
	 * @param index
	 * 			the index of the element to retrieve.
	 * @return
	 * 			the element stored in this catalogue by this index.
	 */
	public Element get(int index) {
		return contents.get(index);
	}
	
	/**
	 * Adds an element to the catalogue.
	 * 
	 * @param e
	 * 			element to add to the catalogue.
	 */
	public void add(Element e) {
		contents.add(e);
		
		if(e.getClass() == Genre.class){
			Genre currGenre = (Genre)e;
		    listOfGenres.put(currGenre.getTitle(), currGenre);
		}
		
		e.setCatalogue(this);
	} 

	/**
	 * Builds a new catalogue and restores its contents from the given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 * @throws FileNotFoundException 
	 * 			  if the provided file cannot be located.
	 */
	public Catalogue(String fileName) throws FileNotFoundException {
		//Create empty storages
		contents = new ArrayList<Element>();
		albumsInGenre = new HashMap<String, HashSet<Element>>();
		albumsByPerformer = new HashMap<String, HashSet<Element>>();
		albumsByTitle = new HashMap<String, HashSet<Element>>();
		
		//Code based on RestoreGenre from source code.
		CharStream stream = new ANTLRInputStream(fileReader(fileName));
		CatalogueLexer lexer = new CatalogueLexer(stream);
		TokenStream tokens = new CommonTokenStream(lexer);
		
		CatalogueParser parser = new CatalogueParser(tokens);
		ParseTree tree = parser.root();
		((RuleContext) tree).inspect(parser);
		
		
		ParseTreeWalker walker = new ParseTreeWalker();
		CatalogueListenerCatalogueCreator listener = new CatalogueListenerCatalogueCreator();
		walker.walk(listener, tree);
		Catalogue c = listener.getCatalogue(); 

		for(int index = 0; index < c.size(); index++) {
			contents.add(c.get(index)); //Add each genre and album to the catalogue being created.
		} 
		
		for(Element e: contents){
			e.setCatalogue(this); //Set each element's catalogue to the catalogue created.
		}
		
	}
	
	//Reads from a file and converts its contents into a string.
	//Throws FileNotFoundException if the file specified cannot be located.
	private String fileReader(String fileName) throws FileNotFoundException{
		Scanner fileReader = new Scanner(new File(fileName));
		String fileContents = "";
		
		while(fileReader.hasNextLine()){
			fileContents = fileContents + fileReader.nextLine() + "\n";
		}
		
		fileReader.close();
		return fileContents;
	}

	/**
	 * Saved the contents of the catalogue to the given file.
	 * Warning: if the file already exists, it will be overwritten.
	 * 
	 * @param fileName
	 *            the file where to save the library
	 * @return 
	 * @throws FileNotFoundException 
	 * 			  if the text file was not successfully created.
	 */
	public void saveCatalogueToFile(String fileName) throws FileNotFoundException {
		PrintWriter catalogue = new PrintWriter(fileName);
		
		for (Element e : contents) {
			catalogue.println(e.toString());
		}
		
		catalogue.close();
	}
}