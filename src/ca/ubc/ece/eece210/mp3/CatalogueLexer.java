package ca.ubc.ece.eece210.mp3;

// Generated from Catalogue.g4 by ANTLR 4.4
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CatalogueLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		START_GENRE=1, END_GENRE=2, START_ALBUM=3, END_ALBUM=4, START_NAME=5, 
		END_NAME=6, START_PERF=7, END_PERF=8, START_YEAR=9, END_YEAR=10, START_SONG=11, 
		END_SONG=12, TEXT=13, WS=14;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'"
	};
	public static final String[] ruleNames = {
		"START_GENRE", "END_GENRE", "START_ALBUM", "END_ALBUM", "START_NAME", 
		"END_NAME", "START_PERF", "END_PERF", "START_YEAR", "END_YEAR", "START_SONG", 
		"END_SONG", "TEXT", "WS"
	};


	public CatalogueLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Catalogue.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\20\u0093\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\6\16\u0089\n\16"+
		"\r\16\16\16\u008a\3\17\6\17\u008e\n\17\r\17\16\17\u008f\3\17\3\17\2\2"+
		"\20\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\3\2\4\5\2\13\f>>@@\3\2\13\f\u0094\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2"+
		"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2"+
		"\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3"+
		"\2\2\2\3\37\3\2\2\2\5\'\3\2\2\2\7\60\3\2\2\2\t8\3\2\2\2\13A\3\2\2\2\r"+
		"H\3\2\2\2\17P\3\2\2\2\21\\\3\2\2\2\23i\3\2\2\2\25p\3\2\2\2\27x\3\2\2\2"+
		"\31\177\3\2\2\2\33\u0088\3\2\2\2\35\u008d\3\2\2\2\37 \7>\2\2 !\7i\2\2"+
		"!\"\7g\2\2\"#\7p\2\2#$\7t\2\2$%\7g\2\2%&\7@\2\2&\4\3\2\2\2\'(\7>\2\2("+
		")\7\61\2\2)*\7i\2\2*+\7g\2\2+,\7p\2\2,-\7t\2\2-.\7g\2\2./\7@\2\2/\6\3"+
		"\2\2\2\60\61\7>\2\2\61\62\7c\2\2\62\63\7n\2\2\63\64\7d\2\2\64\65\7w\2"+
		"\2\65\66\7o\2\2\66\67\7@\2\2\67\b\3\2\2\289\7>\2\29:\7\61\2\2:;\7c\2\2"+
		";<\7n\2\2<=\7d\2\2=>\7w\2\2>?\7o\2\2?@\7@\2\2@\n\3\2\2\2AB\7>\2\2BC\7"+
		"p\2\2CD\7c\2\2DE\7o\2\2EF\7g\2\2FG\7@\2\2G\f\3\2\2\2HI\7>\2\2IJ\7\61\2"+
		"\2JK\7p\2\2KL\7c\2\2LM\7o\2\2MN\7g\2\2NO\7@\2\2O\16\3\2\2\2PQ\7>\2\2Q"+
		"R\7r\2\2RS\7g\2\2ST\7t\2\2TU\7h\2\2UV\7q\2\2VW\7t\2\2WX\7o\2\2XY\7g\2"+
		"\2YZ\7t\2\2Z[\7@\2\2[\20\3\2\2\2\\]\7>\2\2]^\7\61\2\2^_\7r\2\2_`\7g\2"+
		"\2`a\7t\2\2ab\7h\2\2bc\7q\2\2cd\7t\2\2de\7o\2\2ef\7g\2\2fg\7t\2\2gh\7"+
		"@\2\2h\22\3\2\2\2ij\7>\2\2jk\7{\2\2kl\7g\2\2lm\7c\2\2mn\7t\2\2no\7@\2"+
		"\2o\24\3\2\2\2pq\7>\2\2qr\7\61\2\2rs\7{\2\2st\7g\2\2tu\7c\2\2uv\7t\2\2"+
		"vw\7@\2\2w\26\3\2\2\2xy\7>\2\2yz\7u\2\2z{\7q\2\2{|\7p\2\2|}\7i\2\2}~\7"+
		"@\2\2~\30\3\2\2\2\177\u0080\7>\2\2\u0080\u0081\7\61\2\2\u0081\u0082\7"+
		"u\2\2\u0082\u0083\7q\2\2\u0083\u0084\7p\2\2\u0084\u0085\7i\2\2\u0085\u0086"+
		"\7@\2\2\u0086\32\3\2\2\2\u0087\u0089\n\2\2\2\u0088\u0087\3\2\2\2\u0089"+
		"\u008a\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\34\3\2\2"+
		"\2\u008c\u008e\t\3\2\2\u008d\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u008d"+
		"\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0092\b\17\2\2"+
		"\u0092\36\3\2\2\2\5\2\u008a\u008f\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}